import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomersService } from '../customer.service';

@Component({
  selector: 'app-sellcars',
  templateUrl: './sellcars.component.html',
  styleUrl: './sellcars.component.css'
})
export class SellcarsComponent implements OnInit{
  customers:any
  constructor(private router:Router,private service:CustomersService){}
  ngOnInit(): void {
    this.service.getCustomers().subscribe((data:any)=>{
      this.customers=data;
    })
  }
 sell(form:any){
  this.customers.reg=form
 this.service.updateCarId(this.customers).subscribe((data:any)=>{

 })
   alert('Received Send')
   this.router.navigate([''])
 }
}
