    import { Component, OnInit } from '@angular/core';
import {  CustomersService } from '../customer.service';

@Component({
  selector: 'app-show-customers',
  templateUrl: './show-customers.component.html',
  styleUrl: './show-customers.component.css'
})
export class ShowCustomersComponent implements OnInit{
customers:any
  constructor(private service:CustomersService){}
  ngOnInit():void{
    this.service.getCustomers().subscribe((data : any)=>{
      this.customers=data;
      console.log(this.customers)
    })
    } 
    deleteCustomer(CustId:any){
      this.service.deleteCustomers(CustId).subscribe((data: any)=>{
        console.log(data);
      } );
    }
    deleteCust(CustId:any){
      console.log('delete cust id: ' + CustId)
     this.deleteCustomer(CustId);
      // const i=this.customers.findIndex((customer:any)=>{
      //   console.log('customer while finding: ' + customer)
      //   return customer.CustId==CustId
      // })
      for (var c = 0; c < this.customers.length; c++) {
        if (this.customers[c].id===CustId){
          break;
        }
      }
      console.log('Delete the index of '+c)
      this.customers.splice(c,1)
    
    }

}
