import { Component, OnInit } from '@angular/core';
import { CarService } from '../car.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-admincars',
  templateUrl: './admincars.component.html',
  styleUrl: './admincars.component.css'
})
export class AdmincarsComponent implements OnInit{
carsdetails:any
car:any
constructor(private service:CarService,private router:Router){}
  ngOnInit(): void {
    this.getCars();
   
  }
  getCars(){
    this.service.getCarDetails().subscribe((data:any)=>{
      this.carsdetails=data;
      console.log(this.carsdetails)
    })
   }

   deleteDetails(carId:any){
    this.service.deleteCar(carId).subscribe((data:any)=>{
    })
   
   }
  addcars(){
    this.router.navigate(['addcars'])
  }
   updateCar(car:any){
    this.service.setCarDetails(car);
    this.service.getCarDetail().subscribe((data:any)=>{
           this.car=data
    })
    console.log('the getting cars details'+this.car)
    this.router.navigate(['editcar'])
   }

deleteCar(carId:any){
  console.log('carId is '+carId)
  this.deleteDetails(carId);    
   for(var c=0;c<this.carsdetails.length;c++){
    // console.log('the id of cars ',this.carsdetails[0].carId)
    if(this.carsdetails[0].carId===carId){
           break;
    }
   }

   console.log('Delete the index of '+c)
   this.carsdetails.splice(c,1)
}

}
