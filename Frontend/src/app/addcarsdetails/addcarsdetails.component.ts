import { Component, OnInit } from '@angular/core';
import { CarService } from '../car.service';
import { Router } from '@angular/router';
import { CustomersService } from '../customer.service';

@Component({
  selector: 'app-addcarsdetails',
  templateUrl: './addcarsdetails.component.html',
  styleUrl: './addcarsdetails.component.css'
})
export class AddcarsdetailsComponent implements OnInit{
  constructor(private service:CarService,private router:Router,private custservice:CustomersService){}

  ngOnInit(): void {
    
  }
addCarDetails(car:any):any{
  console.log(car)
      this.service.registerCarDetails(car).subscribe(
        (response:any) => {
          console.log('Added Cars details successfully!', response);
          alert("Added Cars Details successfully")
         this.router.navigate(['admincars'])
         this.custservice.setUserLoggedIn();
        },
        (error:any) => {
          console.error('Error adding cars details:', error);
          
        }
      );
}
}
