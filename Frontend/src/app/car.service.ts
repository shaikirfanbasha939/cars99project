import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CarService {
 cardetails:Subject<any>
 enquire:Subject<any>
  constructor(private http:HttpClient) {
    this.cardetails=new Subject<any>()
    this.enquire=new Subject<any>()
   }
  setEnquire(form:any){
    this.enquire.next(form)
  }
  getEnquire():any{
    return this.enquire.asObservable()
  }
   getCarDetail():any{
    return this.cardetails.asObservable();
 }
 setCarDetails(car:any){
  this.cardetails.next(car);
 }
  registerCarDetails(form:any):any{
    return this.http.post("http://localhost:8080/registerCars",form);
  }

  getCarDetails():any{
    return this.http.get("http://localhost:8080/getAllCars");
  }
 deleteCar(carId:any):any{
  return this.http.delete("http://localhost:8080/deleteCar/"+carId)
 }
editCarDetails(form:any):any{
  return this.http.put("http://localhost:8080/editCar",form)
}

}
