import { NgModule } from '@angular/core';
import { BrowserModule, provideClientHydration } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './Components/login/login.component';
import { HeaderComponent } from './Components/header/header.component';
import { RegisterComponent } from './Components/register/register.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './Components/home/home.component';
import { BuyCarComponent } from './buy-car/buy-car.component';
import { LogoutComponent } from './logout/logout.component';
import { ShowCustomersComponent } from './show-customers/show-customers.component';
import { AdmincarsComponent } from './admincars/admincars.component';
import { AddcarsdetailsComponent } from './addcarsdetails/addcarsdetails.component';
import { ShowBookedCarsComponent } from './show-booked-cars/show-booked-cars.component';
import { EditcardetailsComponent } from './editcardetails/editcardetails.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { FooterComponent } from './footer/footer.component';
import { SellcarsComponent } from './sellcars/sellcars.component';
import { DetailsOfCarComponent } from './details-of-car/details-of-car.component';
import { EnquireComponent } from './enquire/enquire.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    RegisterComponent,
    HomeComponent,
    BuyCarComponent,
    LogoutComponent,
    ShowCustomersComponent,
    AdmincarsComponent,
    AddcarsdetailsComponent,
    ShowBookedCarsComponent,
    EditcardetailsComponent,
    ContactUsComponent,
    AboutUsComponent,
    FooterComponent,
    SellcarsComponent,
    DetailsOfCarComponent,
    EnquireComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    HttpClientModule
  ],
  providers: [
    // provideClientHydration()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
