import { Component, OnInit } from '@angular/core';
import { CarService } from '../car.service';
import { CustomersService } from '../customer.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-buy-car',
  templateUrl: './buy-car.component.html',
  styleUrl: './buy-car.component.css'
})
export class BuyCarComponent implements OnInit{
 carsdetails:any
 customer:any
 loginStatus:any;
 constructor(private service:CarService,private custService:CustomersService,private router:Router){
 }
  ngOnInit(): void {
      this.custService.getEmailUser().subscribe((user:any)=>{
       this.customer=user.emailId;
      })
     this.custService.getLoginStatus().subscribe((data: any) => {
      this.getStatus(data);
       console.log("the status of the login "+data)
     });
   
  
    this.getCars();
  }

  viewDetailsOfCars(car:any){
    this.service.setCarDetails(car);
       this.router.navigate(['details'])
     
      
       
  }
  getStatus(data:any){
     this.loginStatus=data;
     console.log("the getStatus of ",this.loginStatus)
  }
  getCars(){
    this.service.getCarDetails().subscribe((data:any)=>{
      this.carsdetails=data;
      console.log(this.carsdetails)
    })
  
   }
  //  getCustEmail(){
  //   console.log('the email is '+this.email)
  //  }

   updateCarIdInCustomer(carId:any){
    // this.getCustEmail()
   this.customer.car=carId;
    this.custService.updateCarId(this.customer).subscribe((data:any)=>{
    })
   }
  navigateLogin(){
    this.router.navigate(['login'])
  }

}
