import { Component, OnInit } from '@angular/core';
import { CustomersService } from '../../customer.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit{
  signUpForm: FormGroup;
  status: any;
  customerName:any
  emailId:any
  password:any
  contactNo:any
  valid:boolean
  ngOnInit(): void {}
  
  constructor(private service:CustomersService,private router:Router,private formBuilder: FormBuilder) {
    this.valid=false
       this.status='';
       this.signUpForm = this.formBuilder.group({
        customerName: ['', Validators.required],
         emailId: ['', [Validators.required, Validators.email]],
         password:['', [Validators.required, Validators.pattern(/^(?=.[a-z])(?=.[A-Z])(?=.\d)(?=.[@$!%?&])[A-Za-z\d@$!%?&]{8}$/)]],
         contactNo: ['', [Validators.required, Validators.pattern(/^\d{10}$/)]],
        
       });
  }
  registerValid(form:any):any{
    this.valid=true;
    console.log(form)
      this.service.registerCustomer(form).subscribe(
        (response:any) => {
          console.log('User registered successfully!', response);
          alert("Registered successfully")
          this.signUpForm.reset()
         this.router.navigate(['login'])
        },
        (error:any) => {
          console.error('Error registering user:', error);
          
        }
      );
    }
    // get password() {
    //   return this.signUpForm.get('password');
    // }

}
