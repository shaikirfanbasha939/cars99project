import { Component, OnInit } from '@angular/core';
import { CustomersService } from '../../customer.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent implements OnInit{
  loginStatus:any;
  userEmail:any;
  adminEmail:any;
  constructor(private service:CustomersService){
  }


  ngOnInit(): void {
     this.service.getEmailUser().subscribe((user:any)=>{
      this.userEmail=user.emailId;
    
     })
    this.service.getEmailAdmin().subscribe((admin:any)=>{
         this.adminEmail=admin;
  
    })
    this.service.getLoginStatus().subscribe((loginStatusData: any) => {
      this.loginStatus = loginStatusData;
    });
  }
 


}
