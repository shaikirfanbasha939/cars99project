import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent {
 brands:any
 constructor(){
  this.brands=[
    {brandImg:"https://www.cars99.com/storage/brands/260423095547-pngwingcom(9).png"},
    {brandImg:"https://www.cars99.com/storage/brands/260423100001-pngwingcom(14).png"},
    // {brandImg:"https://www.cars99.com/storage/brands/260423100517-pngwingcom(16).png"},
    {brandImg:"https://www.cars99.com/storage/brands/260423095638-pngwingcom(10).png"},
    {brandImg:"https://www.cars99.com/storage/brands/260423105613-pngwingcom(29).png"},
    {brandImg:"https://www.cars99.com/storage/brands/260423102458-pngwingcom(19).png"},
    {brandImg:"https://www.cars99.com/storage/brands/260423102706-pngwingcom(21).png"},
    {brandImg:"https://www.cars99.com/storage/brands/260423100839-pngwingcom(18).png"}
    
  ]
 }
}
