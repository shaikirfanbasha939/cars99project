import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomersService } from '../../customer.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit{
  email : any;
  password : any;
  customers : any;
  customer:any;
captcha:any;
  constructor(private router : Router,private service:CustomersService){
    this.email="admin@gmail.com"
    this.password="password"
  }
ngOnInit(): void {
  this.service.getCustomers().subscribe((data:any)=>{
    this.customers=data;
    console.log(this.customers);
    this.generateCaptcha();
  })
}

generateCaptcha(): void {
  const possibleChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const length = 6;
  this.captcha = Array.from({ length }, () =>
    possibleChars[Math.floor(Math.random() * possibleChars.length)]
  ).join('');
}

getCustomer(form:any):any{
  this.service.getCustomer(form).subscribe(
    (data:any)=>{
      console.log('getCustomer: ', data);
      this.customer=data;

    console.log('The data',this.customer)}
  )
   return this.customer;
}
  loginValidate(form:any){
    const email = form.email;
    const password = form.password;
 
    this.service.getCustomer(form).subscribe((data: any) => {
      this.customer = data;
      console.log(data); 
      if (this.customer !== null) {
        alert('Login Successfully! '+this.customer.emailId)
        this.service.setUserLoggedIn();
        this.router.navigate(['']);
        this.service.setUserEmail(this.customer);
       }
      else if(email==this.email&&password==this.password){
        alert('welcome to admin page')
        this.router.navigate(['admincars']);
        this.service.setUserLoggedIn();
        this.service.setAdminEmail(email);
      }
      else {
        alert('Invalid credentials');
        this.router.navigate(['register'])
      } 
    });
  }


}  
    // console.log(form);
    // console.log('email: '+ form.value.email);
    // console.log('password: '+ form.value.password);
    // if(form.email === "admin@gmail.com" && form.password === "password"){
    //   alert ("welcome to admin page")
    //   this.service.setUserLoggedIn();
    //   this.router.navigate(['customers']);
    //   this.router.navigate(['admincars'])
    // }
    // else{
    //    this.customer= this.getCustomer(form)
    //     // this.service.getCustomer(form).subscribe(
    //     //   (data:any)=>{
    //     //     console.log('getCustomer: ', data);
    //     //     this.customer=data;
    //     //   console.log('The data',this.customer)}
    //     // )

    //    if(this.customer==null){
    //    console.log('the data type is '+this.customer==null)
    //     alert("Login failed "+this.customer==null)
    //     this.router.navigate([''])
    //    } else if(this.customer.emailId===form.email){
    //     alert("login success")
      
    //     this.router.navigate(['register'])
    //   }
     //}
     
    
  



