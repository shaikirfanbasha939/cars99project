import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {
  isUserLoggedIn:boolean;
  loginStatus: Subject<any>;
  deleteResult : any;
  emailUser:Subject<any>;
  emailAdmin:Subject<any>;
  constructor(private http:HttpClient) { 
    this.isUserLoggedIn=false;
    this.loginStatus = new Subject<any>(); 
    this.emailUser=new Subject<any>();
    this.emailAdmin=new Subject<any>();
  }
   getEmailUser():any{
      return this.emailUser.asObservable();
   }
   setUserEmail(emailUser:any){
        this.emailUser.next(emailUser);
   }
   getEmailAdmin():any{
    return this.emailAdmin.asObservable();
 }
 setAdminEmail(emailAdmin:any){
      this.emailAdmin.next(emailAdmin);
 }

 getLoginStatus(): any {
    return this.loginStatus.asObservable();   
  }
  setUserLoggedIn(){
    this.isUserLoggedIn=true;
    this.loginStatus.next(true);  
   }
   getUserLoggedStatus():boolean{
    return this.isUserLoggedIn;
   }
   setUserLogout(){
    this.isUserLoggedIn=false;
    this.loginStatus.next(false); 
   }

  registerCustomer(formData:any):any{
    return this.http.post("http://localhost:8080/registerCustomer",formData)
  }
  
  getCustomers(){
    return this.http.get("http://localhost:8080/getAllCustomers");
  }
  getCustomer(form:any): any {
    // return this.http.get('https://localhost:8080/login/' + form.email + "/" + form.password);
    return this.http.get('http://localhost:8080/login/'+form.email+'/'+form.password);
}
deleteCustomers(empId:any):any{
  let url = 'http://localhost:8080/deleteCustomer/'+empId;
  console.log(url);
   return this.http.delete(url)
}
  
updateCarId(customer:any):any{
  return this.http.put("http://localhost:8080/updateCarId",customer);
}

}
