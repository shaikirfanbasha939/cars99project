import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './Components/login/login.component';
import { RegisterComponent } from './Components/register/register.component';
import { BuyCarComponent } from './buy-car/buy-car.component';
import { HomeComponent } from './Components/home/home.component';
import { LogoutComponent } from './logout/logout.component';
import { AuthGuard } from './auth.guard';
import { ShowCustomersComponent } from './show-customers/show-customers.component';
import { AdmincarsComponent } from './admincars/admincars.component';
import { AddcarsdetailsComponent } from './addcarsdetails/addcarsdetails.component';
import { EditcardetailsComponent } from './editcardetails/editcardetails.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { SellcarsComponent } from './sellcars/sellcars.component';
import { DetailsOfCarComponent } from './details-of-car/details-of-car.component';
import { EnquireComponent } from './enquire/enquire.component';
import { ShowBookedCarsComponent } from './show-booked-cars/show-booked-cars.component';

const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'buy',component:BuyCarComponent},
  {path:'login', component:LoginComponent},
  {path:'logout',component:LogoutComponent},
  {path:'register', component:RegisterComponent},
  // {path:'vd',component:VehicledetailsComponent,canActivate:[AuthGuard]},
  {path:'customers',component:ShowCustomersComponent},
  {path:'addcars',component:AddcarsdetailsComponent},
  {path:'admincars',component:AdmincarsComponent},
  {path:'editcar',component:EditcardetailsComponent},
  {path:'aboutus',component:AboutUsComponent},
  {path:'contactus',component:ContactUsComponent},
  {path:'sell',component:SellcarsComponent},
  {path:'details',component:DetailsOfCarComponent},
  {path:'enquire',component:EnquireComponent},
  {path:'sellDetails',component:ShowBookedCarsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
