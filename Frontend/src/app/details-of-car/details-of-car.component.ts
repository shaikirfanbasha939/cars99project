import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CarService } from '../car.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-details-of-car',
  templateUrl: './details-of-car.component.html',
  styleUrl: './details-of-car.component.css'
})
export class DetailsOfCarComponent implements OnInit{
  carDetails:any


  constructor(private router:Router,private service:CarService){
    this.service.getCarDetail().subscribe((data:any)=>{
      this.carDetails=data
      console.log('the details of',this.carDetails)
    })

  
  }
ngOnInit(): void { 
//  this.service.getCarDetail().subscribe((car:any)=>{
//   this.carDetails=car;
//   console.log('the details of',this.carDetails)
//  })
  console.log('the details of',this.carDetails)
}
 enquireDetails(){
  this.router.navigate(['enquire'])
 }
}