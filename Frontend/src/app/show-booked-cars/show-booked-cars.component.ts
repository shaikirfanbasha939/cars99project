import { Component, OnInit } from '@angular/core';
import { CarService } from '../car.service';
import { CustomersService } from '../customer.service';

@Component({
  selector: 'app-show-booked-cars',
  templateUrl: './show-booked-cars.component.html',
  styleUrl: './show-booked-cars.component.css'
})
export class ShowBookedCarsComponent implements OnInit{
  customers:any
    constructor(private service: CustomersService){
      
    }
    ngOnInit(): void {
      this.service.getCustomers().subscribe((data:any)=>{
        this.customers=data
      })
      console.log('the enquire data',this.customers)
    
    }
}
