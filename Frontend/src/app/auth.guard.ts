// import { CanActivateFn } from '@angular/router';

// export const authGuard: CanActivateFn = (route, state) => {
//   return true;
// };
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { CustomersService } from "./customer.service";
@Injectable({   // <-- This is important
  providedIn: 'root'
})
export class AuthGuard {

  constructor(private service:CustomersService,private router:Router) {}

  canActivate(): boolean {
    // Check if the user is logged in and return true
    if(this.service.getUserLoggedStatus()){
      return true;
    }
    // else navigate to login component and return false
    else{
      this.router.navigate(['login']);
      return false;
    }

  }

}
