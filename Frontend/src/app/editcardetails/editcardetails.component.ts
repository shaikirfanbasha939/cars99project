import { Component, OnInit } from '@angular/core';
import { CarService } from '../car.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editcardetails',
  templateUrl: './editcardetails.component.html',
  styleUrl: './editcardetails.component.css'
})
export class EditcardetailsComponent implements OnInit{
  cardetails:any
  constructor(private service:CarService,private router:Router){}
  ngOnInit(): void {
     this.service.getCarDetail().subscribe((car:any)=>{
          this.cardetails=car;
  
     })
     console.log('the car details is '+this.cardetails) 
  }
  
  editCarDetails(form:any){
      this.service.editCarDetails(form).subscribe((data:any)=>{
        console.log('the car details is '+this.cardetails.img) 
      })
  }
}
