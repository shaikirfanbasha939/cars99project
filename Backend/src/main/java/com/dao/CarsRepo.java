package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.models.Cars;
import com.models.Customers;

@Repository
public interface CarsRepo extends JpaRepository<Cars,Integer> {
//	@Query("from Customers,Cars  where Cars.carId= Customers.car")
//	public List<Cars> getAll();
}
