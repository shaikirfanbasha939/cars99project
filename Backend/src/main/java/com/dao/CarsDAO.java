package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.models.Cars;

@Service
public class CarsDAO {
 
	@Autowired
	private CarsRepo carsRepo;
	
	public void register(Cars c1){
		carsRepo.save(c1);
	}
	public List<Cars> getAllCars(){
		return carsRepo.findAll();
	}
	
	public void delete(int id){
		carsRepo.deleteById(id);
	}
	
	public void editCar(Cars c1){
		carsRepo.save(c1);
	}
}
