package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.models.Customers;

@Repository
public interface CustomersRepo extends JpaRepository<Customers,Integer>{

	@Query("select c from Customers c where c.emailId =:emailId and c.password = :password")
	public Customers login(@Param("emailId") String emailId, @Param("password") String password);
	
//	@Modifying
//	@Query("update Customers u set u.car = ?1 where u.id = ?2")
//	public void updateCarId(Integer carId,Integer id);
}