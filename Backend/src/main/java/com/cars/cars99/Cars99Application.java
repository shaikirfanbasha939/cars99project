package com.cars.cars99;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages="com.dao")
@EntityScan(basePackages="com.models")
@SpringBootApplication(scanBasePackages="com")
public class Cars99Application {

	public static void main(String[] args) {
		SpringApplication.run(Cars99Application.class, args);
	}

}
