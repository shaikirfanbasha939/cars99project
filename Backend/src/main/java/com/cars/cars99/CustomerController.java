package com.cars.cars99;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//import com.dao.CarsDAO;
import com.dao.CustomersDAO;
//import com.models.Cars;
import com.models.Customers;
//import com.models.CustomersDTO;

@CrossOrigin(origins="*")
@RestController
public class CustomerController {
	
	@Autowired
	private CustomersDAO customerDao;
	
//	@Autowired
//	private CarsDAO carsDao;
//	
//	
//	@PostMapping("/regCustomer")
//	public Customers registerEmployee(@RequestBody final CustomersDTO custDTO) {
//                // store employee data in Employee object
//		Customers cust = new Customers(custDTO.getId(), custDTO.getCustomerName(), custDTO.getEmailId(), 
//				custDTO.getContactNo(), custDTO.getPassword(), custDTO.getCity(), 
//				custDTO.getState());
//
//                // get all departments to verify if the given deptId exists
//		List<Cars> cars = carsDao.getAllCars();
//		cars.forEach(car -> {
//			int carId = custDTO.getCar();
//			if (car.getCarId() == carId) {
//                                // set department object to emp if deptId is valid
//				cust.setCar(carsDao.getCarId(cars));
//				return;
//			}
//		});
//                // register emp with related Department object
//		return customerDao.registerEmployee(cust);
//	}
//	
//	@PutMapping("/updateEmp")
//	public Employee updateEmployee( @RequestBody EmployeeDTO empDTO ) {
//		return this.registerEmployee(empDTO);
//	}
	
	
     
	@RequestMapping("registerCustomer")
	public void register(@RequestBody Customers c1){
	     customerDao.register(c1);
	}
	
	@RequestMapping("getAllCustomers")
	public List <Customers> getAllCustomers(){
		return customerDao.getAllCustomers();
	}
	
	@GetMapping("/login/{emailId}/{password}")
	public Customers login(@PathVariable("emailId") String emailId, @PathVariable("password") String password) {
		Customers cust = customerDao.login(emailId, password);
		System.out.println(cust);
	    return cust;
	}
	
	@DeleteMapping("deleteCustomer/{id}")
	public String delete(@PathVariable("id") int id){
		customerDao.deleteCustomer(id);
		return "Customer with id " + id + " is deleted.";
	}
	
	@PutMapping("updateCarId")
		public void updateCarId(@RequestBody Customers c1){
		customerDao.updateCarId(c1);
	}
}
