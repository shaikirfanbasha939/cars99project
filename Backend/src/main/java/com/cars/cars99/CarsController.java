package com.cars.cars99;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CarsDAO;
import com.models.Cars;

@CrossOrigin(origins="*")
@RestController
public class CarsController {
    
	@Autowired
	private CarsDAO carsDao;
	
	
	@RequestMapping("registerCars")
	public void register(@RequestBody Cars c1){
		carsDao.register(c1);
	}
	
	@RequestMapping("getAllCars")
	public List<Cars> getAllCars(){
		return carsDao.getAllCars();
	}
	
	@DeleteMapping("deleteCar/{id}")
	public String delete(@PathVariable("id") int id){
		carsDao.delete(id);
		return "Car details with id " + id + " is deleted.";
	}
	 @PutMapping("editCar")
	 public void editCar(@RequestBody Cars c1){
		 carsDao.editCar(c1);
	 }
	 
	
}
