package com.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Cars {
	@Id
	@GeneratedValue
	private int carId;
	
	private String img;
	private String brand;
	private String model;
	private String description;
	private double price;

	
	@Override
	public String toString() {
		return "Cars [carId=" + carId + ", img=" + img + ", brand=" + brand + ", model=" + model + ", description="
				+ description + ", price=" + price + ", custlist=" + custlist + "]";
	}



	@JsonIgnore
	@OneToMany(mappedBy="car")
	List<Customers> custlist=new ArrayList<Customers>();
	
	public Cars() {
		super();
	}

	public Cars(int carId, String img, String brand, String model, String description, double price,String reg) {
		super();
		this.carId = carId;
		this.img = img;
		this.brand = brand;
		this.model = model;
		this.description = description;
		this.price = price;
	
	}


	
	 public List<Customers> getCustlist() {
			return custlist;
		}
	public int getCarId() {
		return carId;
	}

	public void setCarId(int carId) {
		this.carId = carId;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
   

	public void setCustlist(List<Customers> custlist) {
		this.custlist = custlist;
	}


	
	
}
