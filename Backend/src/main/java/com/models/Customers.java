package com.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;



@Entity
public class Customers {
	@Id
	@GeneratedValue
	private int id;
	
	 private String customerName;
     private String emailId;
     private String contactNo;
     private String password;
     private String city;
     private String state;
     private String reg;
     
     
//     @ManyToOne
// 	 @JoinColumn(name="carId")
// 	 Cars car;
     @ManyToOne
     @JoinColumn(name="car")
     Cars car;

	public Customers() {
		super();
	}

	
	

	public Customers(int id, String customerName, String emailId, String contactNo, String password, String city,
			String state,String reg) {
		super();
		this.id = id;
		this.customerName = customerName;
		this.emailId = emailId;
		this.contactNo = contactNo;
		this.password = password;
		this.city = city;
		this.state = state;
		this.reg=reg;
		
	}




	public Cars getCar() {
		return car;
	}

	public String getReg() {
		return reg;
	}

	public void setReg(String reg) {
		this.reg = reg;
	}


	public void setCar(Cars car) {
		this.car = car;
	}




	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}




	@Override
	public String toString() {
		return "Customers [id=" + id + ", customerName=" + customerName + ", emailId=" + emailId + ", contactNo="
				+ contactNo + ", password=" + password + ", city=" + city + ", state=" + state + ", reg=" + reg
				+ ", car=" + car + "]";
	}




	
    


	
 	
}
